import static org.junit.Assert.*;

import org.junit.Test;

public class JamesBondTest {
	@Test
	public void testCase0(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("("));
	}
	@Test
	public void testCase1(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(("));
	}
	@Test
	public void testCase2(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("((("));
	}
	@Test
	public void testCase3(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(()"));
	}
	@Test
	public void testCase4(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "((007)"));
	}
	@Test
	public void testCase5(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("((07)"));
	}
	@Test
	public void testCase6(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("((7)"));
	}
	@Test
	public void testCase7(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("()"));
	}
	@Test
	public void testCase8(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("()("));
	}
	@Test
	public void testCase9(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("())"));
	}
	@Test
	public void testCase10(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "()007)"));
	}
	@Test
	public void testCase11(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("()07)"));
	}
	@Test
	public void testCase12(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("()7)"));
	}
	@Test
	public void testCase13(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0("));
	}
	@Test
	public void testCase14(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0(("));
	}
	@Test
	public void testCase15(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0()"));
	}
	@Test
	public void testCase16(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0(007)"));
	}
	@Test
	public void testCase17(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0(07)"));
	}
	@Test
	public void testCase18(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0(7)"));
	}
	@Test
	public void testCase19(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0)"));
	}
	@Test
	public void testCase20(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0)("));
	}
	@Test
	public void testCase21(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0))"));
	}
	@Test
	public void testCase22(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0)007)"));
	}
	@Test
	public void testCase23(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0)07)"));
	}
	@Test
	public void testCase24(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0)7)"));
	}
	@Test
	public void testCase25(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00("));
	}
	@Test
	public void testCase26(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00(("));
	}
	@Test
	public void testCase27(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00()"));
	}
	@Test
	public void testCase28(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00(007)"));
	}
	@Test
	public void testCase29(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00(07)"));
	}
	@Test
	public void testCase30(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00(7)"));
	}
	@Test
	public void testCase31(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00)"));
	}
	@Test
	public void testCase32(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00)("));
	}
	@Test
	public void testCase33(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00))"));
	}
	@Test
	public void testCase34(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00)007)"));
	}
	@Test
	public void testCase35(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00)07)"));
	}
	@Test
	public void testCase36(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00)7)"));
	}
	@Test
	public void testCase37(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(000("));
	}
	@Test
	public void testCase38(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(000)"));
	}
	@Test
	public void testCase39(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(000007)"));
	}
	@Test
	public void testCase40(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00007)"));
	}
	@Test
	public void testCase41(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0007)"));
	}
	@Test
	public void testCase42(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(001("));
	}
	@Test
	public void testCase43(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(001)"));
	}
	@Test
	public void testCase44(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(001007)"));
	}
	@Test
	public void testCase45(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00107)"));
	}
	@Test
	public void testCase46(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0017)"));
	}
	@Test
	public void testCase47(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(002("));
	}
	@Test
	public void testCase48(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(002)"));
	}
	@Test
	public void testCase49(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(002007)"));
	}
	@Test
	public void testCase50(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00207)"));
	}
	@Test
	public void testCase51(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0027)"));
	}
	@Test
	public void testCase52(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(003("));
	}
	@Test
	public void testCase53(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(003)"));
	}
	@Test
	public void testCase54(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(003007)"));
	}
	@Test
	public void testCase55(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00307)"));
	}
	@Test
	public void testCase56(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0037)"));
	}
	@Test
	public void testCase57(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(004("));
	}
	@Test
	public void testCase58(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(004)"));
	}
	@Test
	public void testCase59(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(004007)"));
	}
	@Test
	public void testCase60(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00407)"));
	}
	@Test
	public void testCase61(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0047)"));
	}
	@Test
	public void testCase62(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(005("));
	}
	@Test
	public void testCase63(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(005)"));
	}
	@Test
	public void testCase64(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(005007)"));
	}
	@Test
	public void testCase65(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00507)"));
	}
	@Test
	public void testCase66(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0057)"));
	}
	@Test
	public void testCase67(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(006("));
	}
	@Test
	public void testCase68(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(006)"));
	}
	@Test
	public void testCase69(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(006007)"));
	}
	@Test
	public void testCase70(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00607)"));
	}
	@Test
	public void testCase71(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0067)"));
	}
	@Test
	public void testCase72(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(007("));
	}
	@Test
	public void testCase73(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(007(("));
	}
	@Test
	public void testCase74(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007()"));
	}
	@Test
	public void testCase75(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007(007)"));
	}
	@Test
	public void testCase76(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007(07)"));
	}
	@Test
	public void testCase77(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007(7)"));
	}
	@Test
	public void testCase78(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)"));
	}
	@Test
	public void testCase79(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)("));
	}
	@Test
	public void testCase80(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)(("));
	}
	@Test
	public void testCase81(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)()"));
	}
	@Test
	public void testCase82(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)(007)"));
	}
	@Test
	public void testCase83(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)(07)"));
	}
	@Test
	public void testCase84(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)(7)"));
	}
	@Test
	public void testCase85(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007))"));
	}
	@Test
	public void testCase86(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007))("));
	}
	@Test
	public void testCase87(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)))"));
	}
	@Test
	public void testCase88(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007))007)"));
	}
	@Test
	public void testCase89(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007))07)"));
	}
	@Test
	public void testCase90(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007))7)"));
	}
	@Test
	public void testCase91(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)0("));
	}
	@Test
	public void testCase92(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)0)"));
	}
	@Test
	public void testCase93(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)0007)"));
	}
	@Test
	public void testCase94(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)007)"));
	}
	@Test
	public void testCase95(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)07)"));
	}
	@Test
	public void testCase96(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)1("));
	}
	@Test
	public void testCase97(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)1)"));
	}
	@Test
	public void testCase98(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)1007)"));
	}
	@Test
	public void testCase99(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)107)"));
	}
	@Test
	public void testCase100(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)17)"));
	}
	@Test
	public void testCase101(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)2("));
	}
	@Test
	public void testCase102(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)2)"));
	}
	@Test
	public void testCase103(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)2007)"));
	}
	@Test
	public void testCase104(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)207)"));
	}
	@Test
	public void testCase105(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)27)"));
	}
	@Test
	public void testCase106(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)3("));
	}
	@Test
	public void testCase107(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)3)"));
	}
	@Test
	public void testCase108(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)3007)"));
	}
	@Test
	public void testCase109(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)307)"));
	}
	@Test
	public void testCase110(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)37)"));
	}
	@Test
	public void testCase111(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)4("));
	}
	@Test
	public void testCase112(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)4)"));
	}
	@Test
	public void testCase113(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)4007)"));
	}
	@Test
	public void testCase114(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)407)"));
	}
	@Test
	public void testCase115(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)47)"));
	}
	@Test
	public void testCase116(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)5("));
	}
	@Test
	public void testCase117(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)5)"));
	}
	@Test
	public void testCase118(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)5007)"));
	}
	@Test
	public void testCase119(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)507)"));
	}
	@Test
	public void testCase120(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)57)"));
	}
	@Test
	public void testCase121(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)6("));
	}
	@Test
	public void testCase122(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)6)"));
	}
	@Test
	public void testCase123(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)6007)"));
	}
	@Test
	public void testCase124(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)607)"));
	}
	@Test
	public void testCase125(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)67)"));
	}
	@Test
	public void testCase126(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)7("));
	}
	@Test
	public void testCase127(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)7)"));
	}
	@Test
	public void testCase128(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)7007)"));
	}
	@Test
	public void testCase129(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)707)"));
	}
	@Test
	public void testCase130(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)77)"));
	}
	@Test
	public void testCase131(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)8("));
	}
	@Test
	public void testCase132(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)8)"));
	}
	@Test
	public void testCase133(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)8007)"));
	}
	@Test
	public void testCase134(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)807)"));
	}
	@Test
	public void testCase135(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)87)"));
	}
	@Test
	public void testCase136(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)9("));
	}
	@Test
	public void testCase137(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)9)"));
	}
	@Test
	public void testCase138(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)9007)"));
	}
	@Test
	public void testCase139(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)907)"));
	}
	@Test
	public void testCase140(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007)97)"));
	}
	@Test
	public void testCase141(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0070("));
	}
	@Test
	public void testCase142(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0070)"));
	}
	@Test
	public void testCase143(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0070007)"));
	}
	@Test
	public void testCase144(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007007)"));
	}
	@Test
	public void testCase145(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00707)"));
	}
	@Test
	public void testCase146(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0071("));
	}
	@Test
	public void testCase147(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0071)"));
	}
	@Test
	public void testCase148(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0071007)"));
	}
	@Test
	public void testCase149(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007107)"));
	}
	@Test
	public void testCase150(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00717)"));
	}
	@Test
	public void testCase151(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0072("));
	}
	@Test
	public void testCase152(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0072)"));
	}
	@Test
	public void testCase153(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0072007)"));
	}
	@Test
	public void testCase154(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007207)"));
	}
	@Test
	public void testCase155(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00727)"));
	}
	@Test
	public void testCase156(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0073("));
	}
	@Test
	public void testCase157(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0073)"));
	}
	@Test
	public void testCase158(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0073007)"));
	}
	@Test
	public void testCase159(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007307)"));
	}
	@Test
	public void testCase160(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00737)"));
	}
	@Test
	public void testCase161(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0074("));
	}
	@Test
	public void testCase162(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0074)"));
	}
	@Test
	public void testCase163(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0074007)"));
	}
	@Test
	public void testCase164(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007407)"));
	}
	@Test
	public void testCase165(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00747)"));
	}
	@Test
	public void testCase166(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0075("));
	}
	@Test
	public void testCase167(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0075)"));
	}
	@Test
	public void testCase168(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0075007)"));
	}
	@Test
	public void testCase169(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007507)"));
	}
	@Test
	public void testCase170(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00757)"));
	}
	@Test
	public void testCase171(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0076("));
	}
	@Test
	public void testCase172(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0076)"));
	}
	@Test
	public void testCase173(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0076007)"));
	}
	@Test
	public void testCase174(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007607)"));
	}
	@Test
	public void testCase175(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00767)"));
	}
	@Test
	public void testCase176(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0077("));
	}
	@Test
	public void testCase177(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0077)"));
	}
	@Test
	public void testCase178(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0077007)"));
	}
	@Test
	public void testCase179(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007707)"));
	}
	@Test
	public void testCase180(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00777)"));
	}
	@Test
	public void testCase181(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0078("));
	}
	@Test
	public void testCase182(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0078)"));
	}
	@Test
	public void testCase183(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0078007)"));
	}
	@Test
	public void testCase184(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007807)"));
	}
	@Test
	public void testCase185(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00787)"));
	}
	@Test
	public void testCase186(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0079("));
	}
	@Test
	public void testCase187(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0079)"));
	}
	@Test
	public void testCase188(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(0079007)"));
	}
	@Test
	public void testCase189(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(007907)"));
	}
	@Test
	public void testCase190(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(00797)"));
	}
	@Test
	public void testCase191(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(008("));
	}
	@Test
	public void testCase192(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(008)"));
	}
	@Test
	public void testCase193(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(008007)"));
	}
	@Test
	public void testCase194(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00807)"));
	}
	@Test
	public void testCase195(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0087)"));
	}
	@Test
	public void testCase196(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(009("));
	}
	@Test
	public void testCase197(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(009)"));
	}
	@Test
	public void testCase198(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(009007)"));
	}
	@Test
	public void testCase199(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(00907)"));
	}
	@Test
	public void testCase200(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0097)"));
	}
	@Test
	public void testCase201(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(01("));
	}
	@Test
	public void testCase202(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(01)"));
	}
	@Test
	public void testCase203(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(01007)"));
	}
	@Test
	public void testCase204(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0107)"));
	}
	@Test
	public void testCase205(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(017)"));
	}
	@Test
	public void testCase206(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(02("));
	}
	@Test
	public void testCase207(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(02)"));
	}
	@Test
	public void testCase208(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(02007)"));
	}
	@Test
	public void testCase209(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0207)"));
	}
	@Test
	public void testCase210(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(027)"));
	}
	@Test
	public void testCase211(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(03("));
	}
	@Test
	public void testCase212(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(03)"));
	}
	@Test
	public void testCase213(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(03007)"));
	}
	@Test
	public void testCase214(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0307)"));
	}
	@Test
	public void testCase215(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(037)"));
	}
	@Test
	public void testCase216(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(04("));
	}
	@Test
	public void testCase217(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(04)"));
	}
	@Test
	public void testCase218(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(04007)"));
	}
	@Test
	public void testCase219(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0407)"));
	}
	@Test
	public void testCase220(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(047)"));
	}
	@Test
	public void testCase221(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(05("));
	}
	@Test
	public void testCase222(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(05)"));
	}
	@Test
	public void testCase223(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(05007)"));
	}
	@Test
	public void testCase224(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0507)"));
	}
	@Test
	public void testCase225(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(057)"));
	}
	@Test
	public void testCase226(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(06("));
	}
	@Test
	public void testCase227(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(06)"));
	}
	@Test
	public void testCase228(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(06007)"));
	}
	@Test
	public void testCase229(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0607)"));
	}
	@Test
	public void testCase230(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(067)"));
	}
	@Test
	public void testCase231(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(07("));
	}
	@Test
	public void testCase232(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(07)"));
	}
	@Test
	public void testCase233(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(07007)"));
	}
	@Test
	public void testCase234(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0707)"));
	}
	@Test
	public void testCase235(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(077)"));
	}
	@Test
	public void testCase236(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(08("));
	}
	@Test
	public void testCase237(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(08)"));
	}
	@Test
	public void testCase238(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(08007)"));
	}
	@Test
	public void testCase239(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0807)"));
	}
	@Test
	public void testCase240(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(087)"));
	}
	@Test
	public void testCase241(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(09("));
	}
	@Test
	public void testCase242(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(09)"));
	}
	@Test
	public void testCase243(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(09007)"));
	}
	@Test
	public void testCase244(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(0907)"));
	}
	@Test
	public void testCase245(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(097)"));
	}
	@Test
	public void testCase246(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(1("));
	}
	@Test
	public void testCase247(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(1)"));
	}
	@Test
	public void testCase248(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(1007)"));
	}
	@Test
	public void testCase249(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(107)"));
	}
	@Test
	public void testCase250(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(17)"));
	}
	@Test
	public void testCase251(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(2("));
	}
	@Test
	public void testCase252(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(2)"));
	}
	@Test
	public void testCase253(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(2007)"));
	}
	@Test
	public void testCase254(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(207)"));
	}
	@Test
	public void testCase255(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(27)"));
	}
	@Test
	public void testCase256(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(3("));
	}
	@Test
	public void testCase257(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(3)"));
	}
	@Test
	public void testCase258(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(3007)"));
	}
	@Test
	public void testCase259(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(307)"));
	}
	@Test
	public void testCase260(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(37)"));
	}
	@Test
	public void testCase261(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(4("));
	}
	@Test
	public void testCase262(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(4)"));
	}
	@Test
	public void testCase263(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(4007)"));
	}
	@Test
	public void testCase264(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(407)"));
	}
	@Test
	public void testCase265(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(47)"));
	}
	@Test
	public void testCase266(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(5("));
	}
	@Test
	public void testCase267(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(5)"));
	}
	@Test
	public void testCase268(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(5007)"));
	}
	@Test
	public void testCase269(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(507)"));
	}
	@Test
	public void testCase270(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(57)"));
	}
	@Test
	public void testCase271(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(6("));
	}
	@Test
	public void testCase272(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(6)"));
	}
	@Test
	public void testCase273(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(6007)"));
	}
	@Test
	public void testCase274(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(607)"));
	}
	@Test
	public void testCase275(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(67)"));
	}
	@Test
	public void testCase276(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(7("));
	}
	@Test
	public void testCase277(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(7)"));
	}
	@Test
	public void testCase278(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(7007)"));
	}
	@Test
	public void testCase279(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(707)"));
	}
	@Test
	public void testCase280(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(77)"));
	}
	@Test
	public void testCase281(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(8("));
	}
	@Test
	public void testCase282(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(8)"));
	}
	@Test
	public void testCase283(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(8007)"));
	}
	@Test
	public void testCase284(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(807)"));
	}
	@Test
	public void testCase285(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(87)"));
	}
	@Test
	public void testCase286(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(9("));
	}
	@Test
	public void testCase287(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(9)"));
	}
	@Test
	public void testCase288(){
	JamesBond test = new JamesBond();
		 assertTrue(test.bondRegex( "(9007)"));
	}
	@Test
	public void testCase289(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(907)"));
	}
	@Test
	public void testCase290(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("(97)"));
	}
	@Test
	public void testCase291(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex(")"));
	}
	@Test
	public void testCase292(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex(")("));
	}
	@Test
	public void testCase293(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("))"));
	}
	@Test
	public void testCase294(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex(")007)"));
	}
	@Test
	public void testCase295(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex(")07)"));
	}
	@Test
	public void testCase296(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex(")7)"));
	}
	@Test
	public void testCase297(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("0("));
	}
	@Test
	public void testCase298(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("0)"));
	}
	@Test
	public void testCase299(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("0007)"));
	}
	@Test
	public void testCase300(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("007)"));
	}
	@Test
	public void testCase301(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("07)"));
	}
	@Test
	public void testCase302(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("1("));
	}
	@Test
	public void testCase303(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("1)"));
	}
	@Test
	public void testCase304(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("1007)"));
	}
	@Test
	public void testCase305(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("107)"));
	}
	@Test
	public void testCase306(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("17)"));
	}
	@Test
	public void testCase307(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("2("));
	}
	@Test
	public void testCase308(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("2)"));
	}
	@Test
	public void testCase309(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("2007)"));
	}
	@Test
	public void testCase310(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("207)"));
	}
	@Test
	public void testCase311(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("27)"));
	}
	@Test
	public void testCase312(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("3("));
	}
	@Test
	public void testCase313(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("3)"));
	}
	@Test
	public void testCase314(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("3007)"));
	}
	@Test
	public void testCase315(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("307)"));
	}
	@Test
	public void testCase316(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("37)"));
	}
	@Test
	public void testCase317(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("4("));
	}
	@Test
	public void testCase318(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("4)"));
	}
	@Test
	public void testCase319(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("4007)"));
	}
	@Test
	public void testCase320(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("407)"));
	}
	@Test
	public void testCase321(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("47)"));
	}
	@Test
	public void testCase322(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("5("));
	}
	@Test
	public void testCase323(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("5)"));
	}
	@Test
	public void testCase324(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("5007)"));
	}
	@Test
	public void testCase325(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("507)"));
	}
	@Test
	public void testCase326(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("57)"));
	}
	@Test
	public void testCase327(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("6("));
	}
	@Test
	public void testCase328(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("6)"));
	}
	@Test
	public void testCase329(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("6007)"));
	}
	@Test
	public void testCase330(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("607)"));
	}
	@Test
	public void testCase331(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("67)"));
	}
	@Test
	public void testCase332(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("7("));
	}
	@Test
	public void testCase333(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("7)"));
	}
	@Test
	public void testCase334(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("7007)"));
	}
	@Test
	public void testCase335(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("707)"));
	}
	@Test
	public void testCase336(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("77)"));
	}
	@Test
	public void testCase337(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("8("));
	}
	@Test
	public void testCase338(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("8)"));
	}
	@Test
	public void testCase339(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("8007)"));
	}
	@Test
	public void testCase340(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("807)"));
	}
	@Test
	public void testCase341(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("87)"));
	}
	@Test
	public void testCase342(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("9("));
	}
	@Test
	public void testCase343(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("9)"));
	}
	@Test
	public void testCase344(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("9007)"));
	}
	@Test
	public void testCase345(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("907)"));
	}
	@Test
	public void testCase346(){
	JamesBond test = new JamesBond();
		 assertFalse(test.bondRegex("97)"));
	}
}